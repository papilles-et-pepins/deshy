# Deshy

Code Arduino pour déshydrateur fait maison !

## Matériel

Le circuit est composé :

- d'un Arduino-Uno ([UNO R3 CH340G](https://robotdyn.com/uno-r3-smt-atmega328-usb-serial-ch340g-micro-usb.html)),
- d'un afficheur 2 lignes x 16 caractères, avec 6 boutons ([LCD KeyPad Shield SKU DFR0009](https://wiki.dfrobot.com/Arduino_LCD_KeyPad_Shield__SKU__DFR0009_))
- d'un capteur d'humidité et de température ([AM2302 DHT22](https://learn.adafruit.com/dht/overview)),
- d'une horloge externe ([DS3231](https://www.maximintegrated.com/en/products/analog/real-time-clocks/DS3231.html)),
- de 3 relais ([SRD-05VDC-SL-C](http://www.datasheetcafe.com/srd-05vdc-sl-c-datasheet-pdf/))

## Librairies externes

Pour compiler le code, il est nécessaire de faire appel à certaines librairies de base fournies par Arduino, ainsi qu'à des librairies externes pour gérer les éléments :

- pour l'afficheur LCD

  - `LiquidCrystal.h` (inclus dans l'IDE Arduino, cf. [documentation](https://www.arduino.cc/en/Reference/LiquidCrystal) et exemples de code [n°1](https://wiki.dfrobot.com/Arduino_LCD_KeyPad_Shield__SKU__DFR0009_) et [n°2](https://wiki.dfrobot.com/LCD_KeyPad_Shield_For_Arduino_SKU__DFR0009))

- pour l'horloge

  - `DS3231.h` ([Github](https://github.com/NorthernWidget/DS3231))

- pour le capteur d'humidité et température

  - `Wire.h` (inclus dans l'IDE Arduino, cf. [documentation](https://www.arduino.cc/en/reference/wire)) est utilisée par la librairie `DS3231.h`
  - `DHT.h` ([Github](https://github.com/adafruit/DHT-sensor-library))
  - `Adafruit_Sensor.h` ([Github](https://github.com/adafruit/Adafruit_Sensor)) qui est une dépendance utilisée par la librairie `DHT.h`
