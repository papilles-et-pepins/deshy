/**
  DEPENDANCES, VARIABLES et CONSTANTES
**/

#define PIN_VENTILO_1 11      // le contrôle de relai de la première série de ventilo est sur la pin 11
#define PIN_VENTILO_2 12      // le contrôle de relai de la deuxième série de ventilo est sur la pin 12
#define PIN_CHAUFFAGE 2       // le contrôle de relai pour l'activation du chauffage est sur la pin PD2
#define PIN_DHT       3       // la récupération des informations provenant de la sonde H/T est sur la pin PD3

// LCD
#include <LiquidCrystal.h>    // Librairie de manipulation du LCD
LiquidCrystal lcd(8,9,4,5,6,7); // Objet LiquidCrystal pour communication avec l'écran LCD
int num_bouton;               // Variable caractérisant la valeur lue du bouton
/** Énumération des boutons utilisables */
enum {
  BUTTON_NONE,  /*!< Pas de bouton appuyé */
  BUTTON_UP,    /*!< Bouton UP (haut) */
  BUTTON_DOWN,  /*!< Bouton DOWN (bas) */
  BUTTON_LEFT,  /*!< Bouton LEFT (gauche) */
  BUTTON_RIGHT, /*!< Bouton RIGHT (droite) */
  BUTTON_MENU,  /*!< Bouton MENU (menu) */
};
String lcd_haut;              // Chaine de caractère pour l'affichage sur la ligne haute du LCD
String lcd_bas;               // Chaine de caractère pour l'affichage sur la ligne basse du LCD

// horloge DS3231
#include <DS3231.h>           // Librairie de manipulation d'horloge : Github https://github.com/NorthernWidget/DS3231
#include <Wire.h>             // Dépendance utile pour DS3231.h
RTClib heure_actuelle;        // Objet en lien avec l'heure actuelle de l'horloge (seule la fonction now() est disponible et renvoie un objet DateTime)
DateTime heure_debut;         // Objet contenant l'heure de début de la chauffe
int duree = 10;               // Variable contenant le temps de chauffe (10h par défaut)

// Sonde Hygrométrie/Température DHT22 (AM2302)
#include <DHT.h>              // Librairie des capteurs DHT  : Github https://github.com/adafruit/DHT-sensor-library
#include <Adafruit_Sensor.h>  // Dépendance utile pour DHT.h : Github https://github.com/adafruit/Adafruit_Sensor
#define DHTTYPE DHT22         // La sonde H/T est une DHT22
DHT dht(PIN_DHT, DHTTYPE);    // Initialisation de l'objet de manipulation de la sonde H/T
int temp_prechauffe = 30;     // variable contenant la température par défaut de préchauffe
int hum_prechauffe = 20;      // variable contenant le taux d'humidité par défaut de préchauffe

int consignes[3] = {duree, temp_prechauffe, hum_prechauffe}; // Consignes de durée (10 heures par défaut), de température (30°C par défaut) et d'humidité (20% par défaut)
int mesures[3] = {0, 0, 0};   // Variables contenant les mesures de température et d'humidité venant du capteur
#define DUREE       0
#define TEMPERATURE 1
#define HUMIDITE    2

// Tableau/matrice pour les étapes du menu de choix de consignes
// [0][0] = 1 : état initial au démarrage, on affiche les valeurs prédéfinies
// [0][1] = 2 : réglage durée
// [0][2] = 3 : réglage température
// [0][3] = 4 : réglage humidité
// [1][0] = 5 : chauffage en cours
// [1][1] = 6 : ajustement durée
// [1][2] = 7 : ajustement température
// [1][3] = 8 : ajustement humidité
int position_menu[2][4] = {1, 2, 3, 4, 5, 6, 7, 8};
#define PRECHAUFFE 0          // Constante définissant la séquence de pré-chauffage
#define CHAUFFE    1          // Constante définissant la séquence de chauffage
#define INITIAL    0          // Constante pour revenir au départ du menu
int mode = PRECHAUFFE;        // Variable qui définit le mode dans lequel on se trouve, initialisée avec PRECHAUFFE correspondant à ma première entrée/colonne de la matrice [0]
int seq  = INITIAL;           // Variable qui définit la séquence dans laquelle on se trouve, initialisée avec INITIAL correspondant à la deuxième colonne de la matrice [0]


/**
 * Fonctions natives Arduino : setup() et loop()
 **/

// Fonction lancée au démarrage
void setup() {
  dht.begin();                   // Initialisation de la sonde  H/T
   
  pinMode(PIN_VENTILO_1,OUTPUT); // Définition de la broche utilisé pour le relai comme étant une sortie
  pinMode(PIN_VENTILO_2,OUTPUT); // Définition de la broche utilisé pour le relai comme étant une sortie
  pinMode(PIN_CHAUFFAGE,OUTPUT); // Définition de la broche utilisé pour le relai comme étant une sortie
   
  lcd.begin(16, 2);              // Initialisation du LCD 2 lignes avec 16 caractères chacune
   
  Wire.begin();                  // Initialisation de la liaison série pour échanger des données avec l'horloge
  heure_debut = heure_actuelle.now();
  
  Serial.begin(9600);            // Démarrage de la liaison série pour débugage
}

// Fonction lancée après setup() et servant de boucle permanente au programme
void loop() {
  /* Mise à jour des données provenant des éléments extérieurs */
  lecture_capteurs();          // Met à jour les valeur de température, d'humidité et le temps passé
  
  /* Mise à jour des consignes en fonction du bouton appuyé */
  maj_consignes();

  /* Envoi des commandes aux relais et autres */
  envoi_commandes();

  /* Gestion du menu */
  affichage_menu();

  /* Temporisation */
  delay(500);                     // 500ms de temporisation avant la prochaine boucle
}

/**
 * Fonctions personnalisées
 **/

// Récupère les données venant des capteurs et de l'horloge
void lecture_capteurs() {
  mesures[TEMPERATURE] = (int)dht.readTemperature();
  mesures[HUMIDITE]    = (int)dht.readHumidity();
  mesures[DUREE]       = calcul_temps_passe();

  Serial.println((String)" - T:"+mesures[TEMPERATURE]+"°C ("+consignes[TEMPERATURE]+"°C) - H: "
                          +mesures[HUMIDITE]+"% ("+consignes[HUMIDITE]+"%) - Tps: "
                          +mesures[DUREE]+"h ("+consignes[DUREE]+"h)\n");
}

// Calcule la différence (en heure) entre deux dates
int calcul_temps_passe() {
  // Note : unixtime est un nombre de seconde (depuis le 1er janvier 1970)
  //        donc si on soustrait deux unixtime, on a le temps (en seconde) entre deux dates
  uint32_t secondes_passees = heure_actuelle.now().unixtime() - heure_debut.unixtime();
  if(secondes_passees > 0){         // Si le nombre de secondes est positif, c'est que du temps a passé
    Serial.print((String)"Durée: "+(secondes_passees / 60)+" min.");
    return ((int)(secondes_passees / 3600));  // nb secondes / 60 / 60 = conversion des secondes en heures
  }
  else                              // Si le nombre de secondes est négatif, c'est qu'on n'a pas commencé
    return -1;
}

// Modifie les consignes en fonction des données des boutons sélectionnés
void maj_consignes() {
  switch(getPressedButton()) {              // Récupération de la valeur indiquant l'appui sur un bouton
    case BUTTON_UP:
      if (seq > 0)
        consignes[seq-1]++;                 // Ajustement de la consigne +1 (seq-1 = 0:durée, 1:température, 2:humidité)
      break;
    case BUTTON_DOWN:
      if (seq > 0)
        consignes[seq-1]--;                 // Ajustement de la consigne -1 (seq-1 = 0:durée, 1:température, 2:humidité)
      break;
    case BUTTON_RIGHT:
      if (mode == PRECHAUFFE && seq == 3) { // Passage au mode CHAUFFE après config des autres valeurs
        mode = CHAUFFE;
        seq = INITIAL;
        heure_debut = heure_actuelle.now(); // Démarrage du compteur de temps
        mesures[DUREE] = 0;
      }
      else if (seq == 3)                    // En mode CHAUFFE retour à la séquence 0 pour lancer la chauffe après (re)définition des consignes
        seq = INITIAL;
      else                                  // Pour les autres cas (seq<3), avancer dans les réglages
        seq++;
      break;
    case BUTTON_LEFT:
      if (seq > 0)
        seq--;                              // On revient à l'ajustement précédent si on n'est pas déjà au premier
      break;
    case BUTTON_MENU:
      if (seq == 0)                         // Arrêt manuel de la chauffe quand on n'est pas en phase de réglage
        mode = PRECHAUFFE;
      else if (mode == CHAUFFE)
        seq = INITIAL;
      break;
  }
  Serial.print((String)"["+mode+":"+seq+"] ");
}

// Retourne le bouton appuyé (si il y en a un)
byte getPressedButton() {
  num_bouton = analogRead(A0);        // Lit l'état des boutons
  if (num_bouton < 900) {
    Serial.print((String)"\nBouton: "+num_bouton);
  }
  
  if (num_bouton < 100) {                // Calcule l'état des boutons en fonction de la valeur numérique renvoyée
    Serial.println(" => Droite");
    return BUTTON_RIGHT;
  }
  else if (num_bouton < 200){
    Serial.println(" => Haut");
    return BUTTON_UP;
  }
  else if (num_bouton < 400){
    Serial.println(" => Bas");
    return BUTTON_DOWN;
  }
  else if (num_bouton < 600){
    Serial.println(" => Gauche");
    return BUTTON_LEFT;
  }
  else if (num_bouton < 900){
    Serial.println(" => Menu");
    return BUTTON_MENU;
  }
  else
    return BUTTON_NONE;
}

void envoi_commandes() {
  Serial.println((String)"Relais: CHAUF="+!digitalRead(PIN_CHAUFFAGE)+" ; VENTIL1="+!digitalRead(PIN_VENTILO_1)+" ; VENTIL2="+!digitalRead(PIN_VENTILO_2));

  digitalWrite(PIN_VENTILO_1,LOW);                              // Peu importe la situation, on ventile tout le temps avec le ventilo 1
  switch(mode) {
    case CHAUFFE:
      if(mesures[DUREE] >= consignes[DUREE]) {                  // Si le temps est dépassé, on passe en mode préchauffe
        mode = PRECHAUFFE;
        seq = INITIAL;
        Serial.println("[chauffe] Passage en mode prechauffe");
        break;
      }

      // Cela permet d'osciller entre + ou -1°C autours de la T°C de consigne afin d'éviter d'allumer/éteindre trop fréquemment la résistance du chauffage
      if (digitalRead(PIN_CHAUFFAGE) == 0 && mesures[TEMPERATURE] < (consignes[TEMPERATURE]+1)) {       // Si la température est inférieure à la consigne+1°C, on chauffe
        digitalWrite(PIN_CHAUFFAGE,LOW);
        Serial.println("[chauffe] Chauffage allumé (oscillation)");
      }
      else if (digitalRead(PIN_CHAUFFAGE) == 1 && mesures[TEMPERATURE] > (consignes[TEMPERATURE]-1)) {  // Si la température est supérieure à la consigne-1°C, on ne chauffe pas
        digitalWrite(PIN_CHAUFFAGE,HIGH);
        Serial.println("[chauffe] Chauffage arrêté (oscillation)");
      }
      else if (mesures[TEMPERATURE] < consignes[TEMPERATURE]) {
        digitalWrite(PIN_CHAUFFAGE,LOW);
        Serial.println("[chauffe] Chauffage allumé");
      }
      else {
        digitalWrite(PIN_CHAUFFAGE,HIGH);
        Serial.println("[chauffe] Chauffage arrêté");
      }
    
      if(mesures[HUMIDITE] > consignes[HUMIDITE]) {             // Si l'humidité est supérieure à la consigne, on ventile double, sinon simple mais on ventile tout le temps
        digitalWrite(PIN_VENTILO_2,LOW);
        Serial.println("[chauffe] Ventilo 2 démarré");
      }
      else {
        digitalWrite(PIN_VENTILO_2,HIGH);
        Serial.println("[chauffe] Ventilo 2 arrêté");
      }
      break;

    case PRECHAUFFE:
      digitalWrite(PIN_VENTILO_2,HIGH);                         // En mode préchauffe, on ne fait tourner qu'un seul ventilateur
      if(mesures[TEMPERATURE] < temp_prechauffe) {              // Si la température est inférieure à la consigne, on chauffe et on ventile avec un seul ventilateur
        digitalWrite(PIN_CHAUFFAGE,LOW);
        Serial.println("[prechauffe] Chauffage allumé");
      }
      else {                                                    // Sinon, on éteint le chauffage (si ce n'est pas déjà fait)
        digitalWrite(PIN_CHAUFFAGE,HIGH);
        Serial.println("[prechauffe] Chauffage arrêté");
      }
      break;
  }
}

void affichage_menu() {
  lcd.clear();
  
  switch(position_menu[mode][seq]) {                          //TODO : définir un mode veille
    case 1:                     // état initial au démarrage : on affiche les valeurs prédéfinies
      lcd_haut = (String)mesures[DUREE] + "h; " + mesures[TEMPERATURE] + "\337C; " + mesures[HUMIDITE] + "% >";
      lcd_bas = String("Reglages ? >OK");
      break;
    case 2:                     // 2 : réglage durée
      lcd_haut = (String)"Duree: " + consignes[DUREE] + "h ?";
      lcd_bas = String("<X  ^+  v-  >OK");
      break;
    case 3:                     // 3 : réglage température
      lcd_haut = (String)"Temp: " + consignes[TEMPERATURE] + "\337C ?";
      lcd_bas = String("<X  ^+  v-  >OK");
      break;
    case 4:                     // 4 : réglage humidité
      lcd_haut = (String)"Humidite: " + consignes[HUMIDITE] + "% ?";
      lcd_bas = String("<X  ^+  v-  >OK");
      break;
    case 5:                     // 5 : chauffage en cours
      lcd_haut = (String)mesures[DUREE] + "h; " + mesures[TEMPERATURE] + "\337C; " + mesures[HUMIDITE] + "%";
      lcd_bas = String("Chauf. en cours");
      break;
    case 6:                     // 6 : ajustement durée
      lcd_haut = (String)"[" + mesures[DUREE] + "h] Duree: " + consignes[DUREE] + "h";
      lcd_bas = String("<X  ^+  v-  >OK");
      break;
    case 7:                     // 7 : ajustement température
      lcd_haut = (String)"[" + mesures[TEMPERATURE] + "\337C] Temp: " + consignes[TEMPERATURE] + "\337C";
      lcd_bas = String("<X  ^+  v-  >OK");
      break;
    case 8:                     // 8 : ajustement humidité
      lcd_haut = (String)"[" + mesures[HUMIDITE] + "%] Hum: " + consignes[HUMIDITE] + "%";
      lcd_bas = String("<X  ^+  v-  >OK");
      break;
  }

  Serial.println();
  
  lcd.home();
  lcd.print(lcd_haut.substring(0,16));
  lcd.setCursor(0,1);
  lcd.print(lcd_bas.substring(0,16));
}
